/*
 * Name : CacheOperation.cpp
 * Author : V1t4m1n
 * Version : 1.0
 */
#include "CacheOperation.hpp"

void CacheOperation::setLibrary(void* library){
	this->library = library;
	this->bi_export = dlsym(library, LIBCRYPTO_EXPORT_FUNCTION);
	this->str_import = dlsym(library, LIBCRYPTO_STR_IMPORT_FUNCTION);
	this->sha1_update = dlsym(library, LIBCRYPTO_SHA1_UPDATE_FUNCTION);
	this->sha256_update = dlsym(library, LIBCRYPTO_SHA256_UPDATE_FUNCTION);
	this->sha256_final = dlsym(library, LIBCRYPTO_SHA256_FINAL_FUNCTION);
	this->sha512_update = dlsym(library, LIBCRYPTO_SHA512_UPDATE_FUNCTION);
	this->md5_update = dlsym(library, LIBCRYPTO_MD5_UPDATE_FUNCTION);
	this->md5_final = dlsym(library, LIBCRYPTO_MD5_FINAL_FUNCTION);
	this->square = dlsym(library, LIBCRYPTO_SQUARE_FUNCTION);
    this->bi_multiply = dlsym(library, LIBCRYPTO_MULTIPLY_FUNCTION);
    this->bi_divide = dlsym(library, LIBCRYPTO_DIVIDE_FUNCTION);
    this->bi_subtract = dlsym(library, LIBCRYPTO_SUBTRACT_FUNCTION);
    this->bi_add = dlsym(library, LIBCRYPTO_ADD_FUNCTION);
}

int CacheOperation::probe(void *addr) {
	/*
     * Input: Address that is to be probed
     * Output: 1 if the value was in the cache, 0 otherwise
     * Probe for an address in order to know if it is already in the cache
     */
	volatile unsigned long time;
	asm __volatile__ (
		" mfence \n"
		" lfence \n"
		" rdtsc \n"
		" lfence \n"
		" movl %%eax, %%esi \n"
		" movl (%1), %%eax \n"
		" lfence \n"
		" rdtsc \n"
		" subl %%esi, %%eax \n"
		" clflush 0(%1) \n"
		: "=a" (time)
		: "c" (addr)
		: "%esi", "%edx");
	if ( time < THRESHOLD ) {
		return 1;
	}
	return 0;
}

void CacheOperation::call(void *addr) {
	/*
     * Input: Address that is to be stored in the cache
     * Output: NULL
     * Perform specific instructions in order to store the given adress in the cache
     */
	asm __volatile__ (
		" lfence \n"
		" movl (%0), %%eax \n"
		: 
		: "c" (addr)
		: "%esi", "%edx");
}

void CacheOperation::A_write_SYN(){
	/*
     * Input: NULL
     * Output: NULL
     * Write the SYN signal in the cache
     */
	for (int i = 0; i < CALL_NB; i++)
	{
 		call((void*) this->md5_update);
	}
 	debug("SYN -->");
}

void CacheOperation::B_write_SYN(){
	/*
     * Input: NULL
     * Output: NULL
     * Write the SYN signal in the cache
     */
	
	for (int i = 0; i < CALL_NB; i++)
	{
		call((void*)this->bi_export);
	}
	debug("SYN -->");
}

void CacheOperation::A_write_ACK(){
	/*
     * Input: NULL
     * Output: NULL
     * Write the ACK signal in the cache
     */
	for (int i = 0; i < CALL_NB; i++)
	{
		call((void*)this->str_import);
	}
	debug("ACK -->");
}

void CacheOperation::B_write_ACK(){
	/*
     * Input: NULL
     * Output: NULL
     * Write the ACK signal in the cache
     */
	for (int i = 0; i < CALL_NB; i++)
	{
		call((void*)this->sha1_update);
	}
	debug("ACK -->");
}

void CacheOperation::A_write_FIN(){
	/*
     * Input: NULL
     * Output: NULL
     * Write the FIN signal in the cache
     */
	for (int i = 0; i < CALL_NB; i++)
	{
		call((void*)this->sha256_update);
	}
	debug("FIN -->");
}

void CacheOperation::B_write_FIN(){
	/*
     * Input: NULL
     * Output: NULL
     * Write the FIN signal in the cache
     */
	for (int i = 0; i < CALL_NB; i++)
	{
		call((void*)this->sha512_update);
	}
	debug("FIN -->");
}

void CacheOperation::write_SYNC(){
	/*
     * Input: NULL
     * Output: NULL
     * Write the SYNC signal in the cache
     */
	for (int i = 0; i < CALL_NB; i++)
	{
		call((void*)this->bi_divide);
	}
	debug("SYNC -->");
}

void CacheOperation::write_ZERO(){
	/*
     * Input: NULL
     * Output: NULL
     * Write the ZERO signal in the cache
     */
	for (int i = 0; i < CALL_NB; i++)
	{
		call((void*)this->square);
		
	}
	debug("ZERO -->");
}

void CacheOperation::write_SYNC_ACK(){
	/*
     * Input: NULL
     * Output: NULL
     * Write the SYNC_ACK signal in the cache
     */
	for (int i = 0; i < CALL_NB; i++)
	{
		call((void*)this->bi_subtract);
		
	}
	debug("SYNC ACK -->");
}

void CacheOperation::write_ONE(){
	/*
     * Input: NULL
     * Output: NULL
     * Write the ONE signal in the cache
     */
	for (int i = 0; i < CALL_NB; i++)
	{
		call((void*)this->bi_add);
	}
	debug("ONE -->");
}

void CacheOperation::write_BIT_ACK(){
	/*
     * Input: NULL
     * Output: NULL
     * Write the BIT_ACK signal in the cache
     */
	for (int i = 0; i < CALL_NB; i++)
	{
		call((void*)this->bi_multiply);
	}
	debug("BIT ACK -->");
}
