/*
 * Name : Transport.cpp
 * Author : V1t4m1n
 * Version : 2.0
 */
#include "Transport.hpp"

Transport::Transport(int mode, callback_rcv_func *callback_rcv){
    /*
     * Input: Running mode (0 for client, 1 for server)
     * Input: Callback called when a message is fully received and decoded.
     * Transport class constructor
     */
    this->callback_rcv = (callback_rcv_func*) callback_rcv;
    this->mode = mode;
    this->cacheoperation = CacheOperation();
    this->initCommFunctions(mode);
    this->cacheoperation.setLibrary(this->library);
    this->received = false;

    if(mode == 0)
        this->state = STATE::STARTED;
    else if(mode == 1)
        this->state = STATE::SYN_WAIT; 
    

    this->listeningThread = new std::thread(Transport::startConnexion, this);
}

void Transport::send(std::vector<std::bitset<8>> data){
    /*
     * Input: Data to be sent to the server in bitsets.
     * Method called to send the data through cache
     */
    this->received = false;
    this->clearReceivedData();
    int data_size = data.size();
    if(!beginSession()) // Sending SYN and waiting for ACK
        error("Unable to begin comm session, no response.");
    
    for(int i = 0; i < data_size; ++i){
        for(int y = 0; y < 8; y++){
            if(!sendBIT(data[i][y]))
                error("Unable to send data, no response.");
        }
    }
    if(!endSession())
        error("Error during session ending, no response.");

}

STATE Transport::getState(){return this->state;}

void Transport::setState(STATE state){this->state = state;}

int Transport::getMode(){return this->mode;}

CacheOperation Transport::getCacheOperation(){return this->cacheoperation;}

bool Transport::isLocked(){
    return this->data_lock;
}

bool Transport::isReceived(){
    return this->received;
}

void Transport::setReceived(bool received){
    this->received = received;
}

std::vector<std::bitset<8>> Transport::getReceivedData(){ 
    while(this->isLocked())
        continue;
    return this->received_data;
}

void Transport::clearReceivedData(){
    this->received_data.clear();
    this->current_bitset.reset();
    this->bitPos = 0;
}

bool Transport::beginSession(){
    /*
     * Begin the communication session (see Communication Protocol)
     */
    int cpt = 0;
    this->setState(STATE::SYN_ACK_WAIT);
    while(this->state == STATE::SYN_ACK_WAIT && cpt <= SIGNAL_SEND_LIMIT){
        ++cpt;
        this->write_SYN();
    }
    if(cpt > SIGNAL_SEND_LIMIT)
        return false;
    return true;
}

bool Transport::sendBIT(int bit){
    /*
     * Input: Bit to be sent (can be 1 or 0).
     * Output: Return true if the bit has been sent correctly, false otherwise
     * Send a bit through the cache
     */
    int cpt = 0;
    this->setState(STATE::SYNC_ACK_WAIT);
    while (this->getState() == STATE::SYNC_ACK_WAIT && cpt <= SIGNAL_SEND_LIMIT)
    {
        cpt++;
        this->getCacheOperation().write_SYNC();
    }
    if(cpt > SIGNAL_SEND_LIMIT)
        return false;
    if(bit == 0)
        {
        info("Sending BIT ZERO.");
        }
        
    else if(bit == 1)
        {
        info("Sending BIT ONE.");
        }
    
    cpt = 0;
    while(this->getState() == STATE::BIT_ACK_WAIT && cpt <= SIGNAL_SEND_LIMIT)
    {
        cpt++;
        if(bit == 0)
        {
        this->getCacheOperation().write_ZERO();
        }
        
        else if(bit == 1)
        {
        this->getCacheOperation().write_ONE();
        }
    }
    if(cpt > SIGNAL_SEND_LIMIT)
        return false;

    return true;
}

void Transport::storeBIT(MESSAGE msg){
    /*
     * Input: Bit to store (can be 1 or 0).
     * Store a bit to the current bits buffer.
     */
    if(msg != MESSAGE::ONE && msg != MESSAGE::ZERO)
        return;
    this->data_lock = true;

    if(msg == MESSAGE::ONE){
        this->current_bitset[bitPos] = true;
        info("Received ONE.");

    }
    else{
        this->current_bitset[bitPos] = false;
        info("Received ZERO.");

    }
    bitPos++;
    if(this->bitPos == 8){
        this->received_data.push_back(std::bitset<8>(this->current_bitset));
        this->current_bitset.reset();
        bitPos = 0;
    }
    this->data_lock = false;
}

bool Transport::endSession(){
    /*
     * End the communication session (see Communication Protocol)
     */
    int cpt = 0;
    this->setState(STATE::FIN_ACK_WAIT);
    while(this->state == STATE::FIN_ACK_WAIT && cpt <= SIGNAL_SEND_LIMIT){
        ++cpt;
        this->write_FIN();
    }
    if(cpt > SIGNAL_SEND_LIMIT)
        return false;
    return true;
}

void Transport::startConnexion(Transport *transport){
    /*
     * Function that listens on received signals and perform specific action depending on those
     * See Communication protocol in the project's wiki for more information
     */
    while(transport->getState() != STATE::STOPPED){
        MESSAGE msg = MESSAGE::NONE;
        msg = Transport::probeForMessage(transport);
        STATE state = transport->getState();
        int mode = transport->getMode();

        switch (msg){
        case MESSAGE::SYN:
            debug("SYN <--");
            transport->write_ACK();
            transport->setState(STATE::SYNC_WAIT);
                
            break;
        case MESSAGE::FIN:
            if(state == STATE::SYNC_WAIT || state == STATE::BIT_WAIT || state == STATE::STARTED || state == STATE::SYN_WAIT){
                debug("FIN <--");
                
                transport->write_ACK();
                transport->callback_rcv(transport);
                transport->setState(STATE::SYN_WAIT);

            }
            break;
        case MESSAGE::ACK:
            if(state == STATE::SYN_ACK_WAIT){
                debug("ACK <--");
                transport->setState(STATE::SYNC_ACK_WAIT);
            }
          
            else if (state == STATE::FIN_ACK_WAIT)
            {
                debug("ACK <--");
                transport->setState(STATE::SYN_WAIT);
            }
            
            break;
        case MESSAGE::SYNC_ACK:
            if (state == STATE::SYNC_ACK_WAIT)
            {
                debug("SYNC ACK <--");
                transport->setState(STATE::BIT_ACK_WAIT);

            }
            break;
        case MESSAGE::BIT_ACK:
            if (state == STATE::BIT_ACK_WAIT)
            {
                debug("BIT ACK <--");
                transport->setState(STATE::SYNC_ACK_WAIT);
            }
            break;
        case MESSAGE::ONE:
        case MESSAGE::ZERO:
                debug("BIT <--");
                transport->getCacheOperation().write_BIT_ACK();
                if(state == STATE::BIT_WAIT)
                    transport->storeBIT(msg);
                transport->setState(STATE::SYNC_WAIT);
            break;
        case MESSAGE::SYNC:
                debug("SYNC <--");
                transport->getCacheOperation().write_SYNC_ACK();
                transport->setState(STATE::BIT_WAIT);
            break;
        default:
            break;
        }
    }
}

MESSAGE Transport::probeForMessage(Transport *transport){
    /*
     * Input : A pointer to the Transport object
     * Output : The detected signal
     * Method that probes on all the signals by looping on them
     */
    int SYN = 0, ACK = 0, FIN = 0, ZERO = 0, ONE = 0, SYNC = 0, SYNC_ACK = 0, BIT_ACK = 0;
    MESSAGE current;
    CacheOperation cacheoperation = transport->getCacheOperation();

    // Probing functions to detect calls
    while(transport->getState() != STATE::STOPPED){
        FIN += cacheoperation.probe(transport->theirFunctions->FIN_Addr);
        if(FIN >= INSTRUCTION_THRESHOLD)
            return MESSAGE::FIN;
        switch(transport->getState()) {
            case STATE::SYN_WAIT:
                SYN += cacheoperation.probe(transport->theirFunctions->SYN_Addr);
                if(SYN >= INSTRUCTION_THRESHOLD)
                    return MESSAGE::SYN;
                break;
            case STATE::SYNC_WAIT:
                SYN += cacheoperation.probe(transport->theirFunctions->SYN_Addr);
                if(SYN >= INSTRUCTION_THRESHOLD)
                    return MESSAGE::SYN;
                // No break here because what is in BIT_WAIT case is included in SYNC_WAIT case
            case STATE::BIT_WAIT:
                SYNC += cacheoperation.probe(transport->theirFunctions->SYNC_Addr);
                ONE += cacheoperation.probe(transport->theirFunctions->ONE_Addr);
                ZERO += cacheoperation.probe(transport->theirFunctions->ZERO_Addr);
                if(SYNC >= INSTRUCTION_THRESHOLD)
                    return MESSAGE::SYNC;
                if(ONE >= BIT_INSTRUCTION_THRESHOLD)
                    return MESSAGE::ONE;
                if(ZERO >= BIT_INSTRUCTION_THRESHOLD)
                    return MESSAGE::ZERO;
                break;
            case STATE::SYNC_ACK_WAIT:
                SYNC_ACK += cacheoperation.probe(transport->theirFunctions->SYNC_ACK_Addr);
                if(SYNC_ACK >= INSTRUCTION_THRESHOLD)
                    return MESSAGE::SYNC_ACK;
                break;
            case STATE::BIT_ACK_WAIT:
                BIT_ACK += cacheoperation.probe(transport->theirFunctions->BIT_ACK_Addr);
                if(BIT_ACK >= INSTRUCTION_THRESHOLD)
                    return MESSAGE::BIT_ACK;
                break;
            case STATE::SYN_ACK_WAIT:
            case STATE::FIN_ACK_WAIT:
                ACK += cacheoperation.probe(transport->theirFunctions->ACK_Addr);
                if(ACK >= INSTRUCTION_THRESHOLD)
                    return MESSAGE::ACK;
                break;
            default:
                return MESSAGE::NONE;
        }
    }
    return MESSAGE::NONE;
}

void Transport::initCommFunctions(int mode){
    /*
     * Input : Communication mode (0 for client, 1 for server)
     * Initialize all the structures by mapping the functions' addresses of the library
     * with the Communication Protocol messages.
     */
    COMM_Functions *A_functions, *B_functions;
    char *dl_error;
    this->library = dlopen(LIBCRYPTO_PATH, RTLD_NOW);
	if (!library) {
        dl_error = dlerror();
		error("dlopen failed: %s",dl_error);
        return;
	}
    A_functions = new COMM_Functions;
    B_functions = new COMM_Functions;

    //Functions' adresses loading

    A_functions->SYN_Addr = dlsym(library, LIBCRYPTO_MD5_UPDATE_FUNCTION);
    if ((dl_error = dlerror()) != NULL)  {
		error("error in dlsym : %s",dl_error);
	}
    A_functions->ACK_Addr = dlsym(library, LIBCRYPTO_STR_IMPORT_FUNCTION);
    if ((dl_error = dlerror()) != NULL)  {
		error("error in dlsym : %s",dl_error);
	}
    A_functions->FIN_Addr = dlsym(library, LIBCRYPTO_SHA256_UPDATE_FUNCTION);
    if ((dl_error = dlerror()) != NULL)  {
		error("error in dlsym : %s",dl_error);
	}
    A_functions->ZERO_Addr = dlsym(library, LIBCRYPTO_SQUARE_FUNCTION);
    if ((dl_error = dlerror()) != NULL)  {
		error("error in dlsym : %s",dl_error);
	}
    A_functions->ONE_Addr = dlsym(library, LIBCRYPTO_ADD_FUNCTION);
    if ((dl_error = dlerror()) != NULL)  {
		error("error in dlsym : %s",dl_error);
	}
    A_functions->SYNC_Addr = dlsym(library, LIBCRYPTO_DIVIDE_FUNCTION);
    if ((dl_error = dlerror()) != NULL)  {
		error("error in dlsym : %s",dl_error);
	}
    A_functions->SYNC_ACK_Addr = dlsym(library, LIBCRYPTO_SUBTRACT_FUNCTION);
    if ((dl_error = dlerror()) != NULL)  {
		error("error in dlsym : %s",dl_error);
	}
    A_functions->BIT_ACK_Addr = dlsym(library, LIBCRYPTO_MULTIPLY_FUNCTION);
    if ((dl_error = dlerror()) != NULL)  {
		error("error in dlsym : %s",dl_error);
	}
    A_functions->SYNC_ACK_Addr = dlsym(library, LIBCRYPTO_SUBTRACT_FUNCTION);
    if ((dl_error = dlerror()) != NULL)  {
		error("error in dlsym : %s",dl_error);
	}

    B_functions->SYN_Addr = dlsym(library, LIBCRYPTO_EXPORT_FUNCTION);
    if ((dl_error = dlerror()) != NULL)  {
		error("error in dlsym : %s",dl_error);
	}
    B_functions->ACK_Addr = dlsym(library, LIBCRYPTO_SHA1_UPDATE_FUNCTION);
    if ((dl_error = dlerror()) != NULL)  {
		error("error in dlsym : %s",dl_error);
	}
    B_functions->FIN_Addr = dlsym(library, LIBCRYPTO_SHA512_UPDATE_FUNCTION);
    if ((dl_error = dlerror()) != NULL)  {
		error("error in dlsym : %s",dl_error);
	}
    B_functions->ZERO_Addr = dlsym(library, LIBCRYPTO_SQUARE_FUNCTION);
    if ((dl_error = dlerror()) != NULL)  {
		error("error in dlsym : %s",dl_error);
	}
    B_functions->ONE_Addr = dlsym(library, LIBCRYPTO_ADD_FUNCTION);
    if ((dl_error = dlerror()) != NULL)  {
		error("error in dlsym : %s",dl_error);
	}
    B_functions->SYNC_Addr = dlsym(library, LIBCRYPTO_DIVIDE_FUNCTION);
    if ((dl_error = dlerror()) != NULL)  {
		error("error in dlsym : %s",dl_error);
	}
    B_functions->SYNC_ACK_Addr = dlsym(library, LIBCRYPTO_SUBTRACT_FUNCTION);
    if ((dl_error = dlerror()) != NULL)  {
		error("error in dlsym : %s",dl_error);
	}
    B_functions->BIT_ACK_Addr = dlsym(library, LIBCRYPTO_MULTIPLY_FUNCTION);
    if ((dl_error = dlerror()) != NULL)  {
		error("error in dlsym : %s",dl_error);
	}

    // Affect structures with functions addresses' mapping depending on mode (Client or Server)

    if (mode == 0){
        this->myFunctions = A_functions;
        this->theirFunctions = (COMM_Functions*) B_functions;
    }
    else if (mode == 1){
        this->myFunctions = B_functions;
        this->theirFunctions = (COMM_Functions*) A_functions;
    }
}

void Transport::write_SYN(){
    if(this->mode == 0)
        this->cacheoperation.A_write_SYN();
    else if(this->mode == 1)
        this->cacheoperation.B_write_SYN();
    return;
}

void Transport::write_ACK(){
    if(this->mode == 0)
        this->cacheoperation.A_write_ACK();
    else if(this->mode == 1)
        this->cacheoperation.B_write_ACK();
    return;
}

void Transport::write_FIN(){
    if(this->mode == 0)
        this->cacheoperation.A_write_FIN();
    else if(this->mode == 1)
        this->cacheoperation.B_write_FIN();
}

Transport::~Transport(){
    this->setState(STATE::STOPPED);
    this->listeningThread->join();
    delete this->myFunctions;
    delete this->theirFunctions;
    delete this->listeningThread;
    dlclose(this->library);
}
