# State diagrams

## Client mode

```mermaid
stateDiagram
    [*] --> STARTED: +new()
    STARTED         --> SYN_ACK_WAIT: SYN ->
    SYN_ACK_WAIT    --> Sending_data: -> ACK - SYNC ->
    state Sending_data {
        SYNC_ACK_WAIT   --> BIT_ACK_WAIT: -> SYNC_ACK - ONE/ZERO ->
        BIT_ACK_WAIT    --> SYNC_ACK_WAIT: -> BIT_ACK - SYNC ->
    }
    Sending_data    --> FIN_ACK_WAIT: -> FIN
    FIN_ACK_WAIT    --> SYN_WAIT: -> ACK
    SYN_WAIT        --> Receiving_data: -> SYN - ACK ->
    state Receiving_data {
    SYNC_WAIT       --> BIT_WAIT: -> SYNC - SYNC_ACK ->
    BIT_WAIT        --> SYNC_WAIT: -> ONE/ZERO - BIT_ACK ->
    }
    Receiving_data       --> STARTED: -> FIN - ACK ->
    
```

## Server mode

```mermaid
stateDiagram
    [*] --> SYN_WAIT: -> +new()
    SYN_WAIT        --> Receiving_data: -> SYN - ACK ->

    state Receiving_data {
    SYNC_WAIT       --> BIT_WAIT: -> SYNC - SYNC_ACK ->
    BIT_WAIT        --> SYNC_WAIT: -> ONE/ZERO - BIT_ACK ->
    }
    Receiving_data  --> SYN_RDY: -> FIN - ACK ->
    SYN_RDY         --> SYN_ACK_WAIT: SYN ->
    SYN_ACK_WAIT    --> Sending_data: -> ACK - SYNC ->
    state Sending_data {
        SYNC_ACK_WAIT   --> BIT_ACK_WAIT: -> SYNC_ACK - ONE/ZERO ->
        BIT_ACK_WAIT    --> SYNC_ACK_WAIT: -> BIT_ACK - SYNC ->
    }
    Sending_data    --> FIN_ACK_WAIT: -> FIN
    FIN_ACK_WAIT    --> SYN_WAIT: -> ACK
```
