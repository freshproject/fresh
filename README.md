# FLUSH+RELOAD Extended Shell

> Disclaimer: this project exploits a vulnerability that exists in most recent
CPUs. It has been made with an educational purpose and, therefor, is not
intented to be used in other way. 

Fresh is a remote shell that uses the FLUSH+RELOAD principle to break process
isolation and communicate between containers (and soon virtual machines!).
It sends commands over the L3 cache (LLC) and retrieves the output via the same
channel, therefor establishing a half-duplex channel.

![A quick fresh demonstration](./fresh.gif)

See [the documentation](https://gitlab.com/freshproject/fresh/-/wikis/FRESH-:-Installation-and-Launch)
to get it running. You can read about the project in the
[wiki](https://gitlab.com/freshproject/fresh/-/wikis/home) section.
